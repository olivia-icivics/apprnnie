#' @importFrom dplyr %>%
aa_req <- function(key, path, params = list()) {
  base_url <- "https://api.appannie.com"

  GET(modify_url(base_url, path = paste0("v1.2", path)),
      add_headers(Authorization = paste("bearer", key)),
      query = params) %>%
    stop_for_status() %>%
    content(as = "parsed")
}

#' Get App Annie Accounts
#'
#' @param key App Annie API key
#'
#' @export
aa_getAccounts <- function(key) {
  accounts <- aa_req(key, "/accounts")

  accounts$accounts %>%
    map_df(as_tibble)
}

#' @export
aa_getProducts <- function(key, account_id = "449304") {
  products <- aa_req(key, paste0("/accounts/", account_id, "/products"))

  products$products %>%
    map_df(as_tibble) %>%
    mutate(account_id = account_id)
}


aa_getDailyFeatures <- function(
  key, vertical = "apps", market = "google-play", product = "20600006428706",
  start_date = "2017-07-01", end_date = "2017-07-14") {

  daily_features <- aa_req(key,
                           paste0("/", vertical,
                                  "/", market,
                                  "/app",
                                  "/", product,
                                  "/featured"),
                           list(start_date = start_date,
                                end_date   = end_date,
                                page_index = 0
                           ))

  daily_features$daily_features %>%
    map_df(as_tibble) %>%
    mutate(vertical = vertical,
           market   = market,
           product  = product)
}

#' @importFrom tibble tibble as_tibble
#' @importFrom tidyr unnest
#' @export
aa_getMarkets <- function(key) {
  markets <- aa_req(key, "/meta/markets")

  markets$verticals %>%
    map_df(as_tibble) %>%
    mutate(markets = map(markets, as_tibble)) %>%
    unnest(markets)

  # markets$verticals %>%
  #   map(function(x) {map(x, function(y) {map(y, as_tibble)})}) %>%
  #   map(as_tibble) %>%
  #   bind_rows() %>%
  #   unnest()
  # markets$verticals %>%
  #   do.call(rbind, .) %>% as_tibble() %>%
  #   unnest(vertical_name, vertical_code) %>%
  #   mutate(markets = map(markets, )) %>%
  #   unnest(markets) #%>%
  #   # unnest(market_name, market_code)
}

#' @export
aa_getCountries <- function(key) {
  countries <- aa_req(key, "/meta/countries")

  countries$country_list %>%
    map_df(as_tibble)
}

#' @export
aa_getFeatureCategories <- function(key, market = "google-play",
                                    countries = list("us")) {
  warning("Not implemented", call. = T)

  feature_categories <- aa_req(key, "/meta/feature/categories",
                               list(market    = "google-play",
                                    countries = "us"))

  feature_categories$all_category_pages %>%
    map_df(as_tibble) %>%
    mutate(category_pages = map(category_pages, as_tibble)) %>%
    unnest(category_pages) %>%
    mutate(market = market)
}

#' @export
aa_getFeatureTypes <- function(key, market = "google-play") {
  warning("Not implemented", call. = T)

  feature_types <- aa_req(key, "/meta/feature/types",
                          list(market = "google-play"))

  feature_types$types %>%
    map_df(as_tibble) %>%
    mutate(market = market)
}

# aa_getFeaturesDaily <- function(key) {
#   # markets   <- aa_getMarkets(key)
#   # countries <- aa_getCountries(key)
#
#   warning("Not implemented", call. = T)
#
#   feature_types <- aa_req(key, "/app/google-play/{asset}/{product_id}/featured",
#                           list(market = "google-play"))
#
#   feature_types$types %>%
#     map_df(as_tibble)
# }

aa_getProductSales <- function(key, account_id = "449304",
                               product_id = "20600006428706") {
  product_sales <- aa_req(key,
                          paste0("/accounts",
                                 "/", account_id,
                                 "/products",
                                 "/", product_id,
                                 "/sales"),
                          list(break_down = "date+country"))

  product_sales$sales_list %>%
    do.call(rbind, .) %>% as_tibble() %>%
    unnest(date, country) %>% select(-revenue) %>%
    mutate(product = map(units, "product") %>% map(as_tibble)) %>%
    unnest(product) %>%
    mutate(account_id = account_id,
           product_id = product_id)
}
